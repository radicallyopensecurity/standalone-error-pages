import concurrently from 'concurrently'
import { dirname } from 'path'
import { fileURLToPath } from 'url'

const cwd = dirname(fileURLToPath(import.meta.url))

const main = async () => {
  const { result } = concurrently([
    {
      command: 'npm run server',
      name: 'server',
      cwd,
      prefixColor: 'blue',
    },
    {
      command: 'nodemon --config ./nodemon.json',
      name: 'build',
      cwd,
      prefixColor: 'magenta',
    },
  ])

  await result
}

main()
