import Handlebars from 'handlebars'
import { readFileSync, writeFileSync, mkdirSync, existsSync } from 'node:fs'
import { extname } from 'node:path'
import { pages } from './src/pages.js'

// helpers

/**
 * Base64 encode a file
 * @param {string} pathToFile
 * @param {string} type
 * @returns string
 */
const toBase64 = (pathToFile, type) =>
  `data:${type}/${extname(pathToFile).split('.')[1]};base64,${readFileSync(
    pathToFile
  ).toString('base64')}`

/**
 * Replace assets with inlined elements
 * @param {string} htmlString
 * @returns string
 */
const replaceAssets = (htmlString) =>
  [
    [
      /<img[^>]*\bsrc="([^">]+\.png)"/g,
      (_, filePath) => `<img src="${toBase64(`src/${filePath}`, 'image')}"`,
    ],
    [
      /href="([^"]+\.png)"/g,
      (_, filePath) => `href="${toBase64(`src/${filePath}`, 'image')}"`,
    ],
    [
      /<img[^>]+src="([^">]+\.svg)"[^>]*>/g,
      (_, filePath) => readFileSync(`src/${filePath}`, 'utf8'),
    ],
    [
      /url\('([^']+\.woff2)'\)/g,
      (_, filePath) => `url('${toBase64(`src/${filePath}`, 'font')}')`,
    ],
  ].reduce((acc, [regex, fn]) => acc.replace(regex, fn), htmlString)

/**
 * Capitalize word
 * @param {string} str
 * @returns string
 */
const capitalize = (str) => `${str.charAt(0).toUpperCase()}${str.slice(1)}`

/**
 * Convert string to Capital Case
 * @param {string} str
 * @returns string
 */
const capitalCase = (str) => str.split(' ').map(capitalize).join(' ')

// build

if (!existsSync('dist')) {
  mkdirSync('dist')
}

Handlebars.registerHelper('capitalCase', capitalCase)

const template = Handlebars.compile(readFileSync('src/template.html', 'utf8'))

pages.forEach((page) => {
  writeFileSync(`dist/${page.code}.html`, replaceAssets(template(page)), 'utf8')
})
