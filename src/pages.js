const DEFAULT_TEXT = `Oops, something went wrong! Our engineers have been notified and
we're looking into it.`

export const pages = [
  {
    code: 500,
    codeText: 'internal server error',
    image: '500.png',
    text: DEFAULT_TEXT,
  },
  {
    code: 502,
    codeText: 'bad gateway',
    image: '502.png',
    text: DEFAULT_TEXT,
  },
  {
    code: 503,
    codeText: 'service unavailable',
    image: '503.png',
    text: `Hello! We're currently performing maintenance. Please check back in 5-10 minutes.`,
  },
  {
    code: 504,
    codeText: 'gateway timeout',
    image: '504.png',
    text: DEFAULT_TEXT,
  },
]
