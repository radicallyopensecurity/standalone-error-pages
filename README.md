# standalone-error-pages

Dependencies

- NodeJS >= 20.x
- NPM >= 10

## Quick Start

```sh
npm install
npm run build
```

The built files will be in the `dist` folder.

## Development

```sh
npm install
npm run dev
```

The error pages will be available at <http://localhost:3000>

## Docker

You can use the `node` docker image to build the project or run the dev server if you don't want to install NodeJS on your machine.

There are convenience bash scripts that run docker placed in the root folder:

```sh
sh docker-install.sh
sh docker-build.sh
sh docker-dev.sh
```

## License

[GPLv2](./LICENSE)
