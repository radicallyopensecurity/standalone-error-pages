#!/bin/sh

docker run \
  -it \
  --rm \
  -v "$PWD":/app \
  -w /app \
  node:latest \
  npm install